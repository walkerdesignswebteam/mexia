[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	<div class="container">
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
		<div class="row">
			<main id="content" role="main" class="col-xs-12">
				[[*content]]
				[[!CommonTools? &cmd=`loadChunk` &name=`ContactForm`]]
			</main>
			<aside class="col-xs-12">
				aside here
			</aside>
			[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	
		</div><!-- /row -->
	</div><!-- /container -->
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>