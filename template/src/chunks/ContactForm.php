[[!JsonFormBuilder-fromJSON? &json=`
{
"id": "ContactForm",
"redirectDocument": "8",
"jqueryValidation":true,
"placeholderJavascript":"JsonFormBuilder_ContactForm",

"emailToAddress": "[[++emailsender]]",
"emailFromAddress": "[[+postVal.email_address]]",
"emailFromName": "[[++site_name]] website",
"emailSubject": "Contact form submission",
"emailHeadHtml": "<p>This is a submission from the [[++site_name]] website:</p>",

"elements": [
"<fieldset>",
{"element":"hidden", "id":"user_group", "label":"User Group", "defaultVal":3},

{"element":"text", "id":"name_full", "label":"Your Name", "extraClasses":["half"],"rules":["required"] },

{"element":"text", "id":"email_address", "label":"Email Address", "extraClasses":["half"], "rules":["required","email"] },

{"element":"textArea", "id":"message", "label":"Message", "rows":5,"columns":30},

{"element":"button", "id":"submit", "label":"Submit Form", "type":"Submit"},

"</fieldset>"
]
}
`]]
<!-- 
<script type="text/javascript">
// <![CDATA[
[[+JsonFormBuilder_ContactForm]]
// ]]>
</script> -->