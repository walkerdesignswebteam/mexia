<?php

class SEOSummary{
    
    private $modx;
    
    public function __construct(&$modx) {
        $this->modx = $modx;
    }
    
    public function outputSummary(){
        $a_fields = $this->getSummary();
        $this->outputCSV($a_fields);
    }
    
    private function getSummary(){
        $a_ret = array();
        $a_ret[] = array('Resource ID','Page Name','Page Name Length','Meta Description','Meta Description Length','URL');
        $c_resources = $this->modx->getCollection('modResource',array('published'=>1));
        $url = $this->modx->getOption('site_url');
        foreach($c_resources as $resource){
            $a_ret[] = array($resource->id,$resource->pagetitle,strlen($resource->pagetitle),$resource->description,strlen($resource->description),$url.$resource->alias);
        }
        return $a_ret;
    }
    
    private function outputCSV($a_fields){
        $tempName = tempnam(MODX_CORE_PATH.'components/b-e/temp/', '');
        $tempFile = fopen($tempName, 'r+');
        foreach($a_fields as $fields){
            fputcsv($tempFile, $fields);
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=SEOSummary.csv');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($tempFile));
        ob_clean();
        flush();

        readfile($tempName);
        exit;
    }
    
}
