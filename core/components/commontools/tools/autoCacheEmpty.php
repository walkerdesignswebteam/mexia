<?php

//create plugin called "autoClearCache" attached to the system event "OnWebPagePrerender"
$templateSrc = MODX_BASE_PATH . 'template/src/';
$modFile = $templateSrc . '.lastmod';
$lastMod = file_get_contents($modFile);
$GLOBALS['lastTemplateSrcModTime'] = 0;

function dir_walk($dir) {
	$dh = opendir($dir);
	if ($dh) {
		while (($file = readdir($dh)) !== false) {
			if ($file === '.' || $file === '.lastmod' || $file === '..') {
				continue;
			}
			if (is_file($dir . $file)) {
				$fileTime = filemtime($dir . $file);
				if ($fileTime > $GLOBALS['lastTemplateSrcModTime']) {
					$GLOBALS['lastTemplateSrcModTime'] = $fileTime;
				}
			} elseif (is_dir($dir . $file)) {
				dir_walk($dir . $file . DIRECTORY_SEPARATOR);
			}
		}
		closedir($dh);
	}
}

dir_walk($templateSrc);
if ($lastMod === false || $lastMod < $GLOBALS['lastTemplateSrcModTime']) {
	file_put_contents($modFile, $GLOBALS['lastTemplateSrcModTime']);
	$resource = $modx->getObject('modResource');
	$modx->cacheManager->refresh(array('resource' => array()));
         echo '<html>Reloading...<script>window.location.reload();</script></html>'; exit();		
}
?>