<?php
////////////////////////////////////////
//VERSION 1.45                        //
//Last Updated: 21/04/2015            //
//Author: Walker Designs              //
////////////////////////////////////////
/*
WALKER DESIGNS COPYRIGHT
------------------------
This code is copyright by Walker Designs. Walker Designs grants you licence to use this
code on your website. You are not licenced to share or modify this code or allow access
to this code to external parties.
*/
class CommonTools{
    public $modx;
	private $parentIds;
	
	/**
	 * Main constructor
	 * @param type $modx 
	 */
	public function CommonTools(&$modx){
    	$this->modx = $modx;
		$this->resourceID=$this->modx->resource->get('id');
		$this->parentIds=$this->modx->getParentIds($this->resourceID);
    }
	public function loadTemplate($template,$jopts){
		$fileToLoad = str_replace('\\','/',dirname(dirname(dirname(dirname(__FILE__)))).'/template/src/templates/'.$template.'.php');
		ob_start();
		$modx = $this->modx; // create variable to be used by the require script;
		if(empty($jopts)===false){
			$json_options = json_decode($jopts,true);
                        if(empty($json_options)){
                            echo '--- template "'.$template.'" json_options was processed with no data (probably json format error) ---';
                        }
		}
		if(file_exists($fileToLoad)===true){
			require $fileToLoad;
		}else{
			echo '--- template "'.$template.'" not found ---';
		}
		$s = ob_get_contents();
		ob_end_clean();
		return $s;
	}
	
	public function loadChunk($template,$jopts){
            
		$fileToLoad = str_replace('\\','/',dirname(dirname(dirname(dirname(__FILE__)))).'/template/src/chunks/'.$template.'.php');
		ob_start();
		$modx = $this->modx; // create variable to be used by the require script;
		if(empty($jopts)===false){
			$json_options = json_decode($jopts,true);
                        if(empty($json_options)){
                            echo '--- chunk "'.$template.'" json_options was processed with no data (probably json format error) ---';
                        }
		}
                
		if(file_exists($fileToLoad)===true){
			require $fileToLoad;
		}else{
			echo '--- chunk "'.$template.'" not found ---';
		}
		$s = ob_get_contents();
		ob_end_clean();
		return $s;
	}
	
	public function getDocChildren($pageID, $orderField='menuindex',$orderDirection='ASC',$showUnpublished=false){
		if($orderDirection!='ASC' && $orderDirection!='DESC'){
			$orderDirection='ASC';
		}
		$a_conditions = array('parent' => $pageID, 'deleted'=>0);
		if($showUnpublished===false){
			$a_conditions['published']=1;
		}
		$c = $this->modx->newQuery('modResource');
		$c->sortby($orderField,$orderDirection);
		$c->where($a_conditions);
  		$children = $this->modx->getCollection('modResource',$c);
		return $children;
	}
	
	/**
	* Get a template variable from a specific document
	* EXAMPLE: [[CommonTools? &cmd=`getTemplateVar` &tvName=`prod-img` &pageID=`130`]]
	*
	* @param string $tvName Template variable name
	* @param integer $pageID Id of the page to retrieve the template varible from
	* @return String Returns a string with the template variables value (or blank string if not found).
	*/
	public function getTemplateVar($tvName,$pageID){
		$o_doc = $this->modx->getObject('modResource',$pageID);
		if($o_doc===NULL){
			return '';
		}else{
			$tvValue = $o_doc->getTVValue($tvName);
			return $tvValue;
		}
	}
        
        /**
	* Determine if mobile or not
	* EXAMPLE: [[!CommonTools? &cmd=`breadCrumbs` &show_home=`1` &sep=`<li><span class="sep"></span></li>`]]
	*
	* @return String Returns 1 or 0 (Obviously returns 1 if mobile device is detected)
	*/
	public function breadCrumbs($sep,$show_home){
            $s_ret='';
            $i_homeResource = $this->modx->getObject('modSystemSetting', 'site_start')->get('value');
            
            //dont make breadcrumbs if on home page.
            if($i_homeResource==$this->modx->resource->id){
                return '';
            }
            
            $a_parentsInit = $this->modx->getParentIds($this->modx->resource->id);
            array_pop($a_parentsInit);
            if($show_home==1){
                $a_parentsInit[]=$i_homeResource;
            }
            $a_parents = array_reverse($a_parentsInit);
            $a_parents[]=$this->modx->resource->id;

            $a_finalCrumbs = array();
            foreach($a_parents as $resourceId){
                 $resource = $this->modx->getObject('modResource',array('id'=>$resourceId,'published'=>1,'hidemenu'=>0));
                 if(!empty($resource)){
                     $s_menuTitle = $resource->menutitle;
                     if(empty($s_menuTitle)){
                         $s_menuTitle = $resource->pagetitle;
                     } 
                     $a_finalCrumbs[]=array('title'=>$s_menuTitle,'id'=>$resourceId);

                 }
            }
            
            if(count($a_finalCrumbs)>0){
                 $s_ret.='<ol class="breadcrumbs">';
                 for($i=0;$i<count($a_finalCrumbs);$i++){
                    $resourceInfo = $a_finalCrumbs[$i];
                    if($i!==0){
                        $s_ret.=$sep;
                    }
                    $s_ret.='<li class="item'.($i==0?' first':'').($i==count($a_finalCrumbs)-1?' last':'').'">';
                        if($this->modx->resource->id!=$resourceInfo['id']){
                            $s_ret.='<a href="[[~'.$resourceInfo['id'].']]">';
                        }
                            $s_ret.=$resourceInfo['title'];
                        if($this->modx->resource->id!=$resourceInfo['id']){
                            $s_ret.='</a>';
                        }
                        
                    $s_ret.='</li>';
                 }
                 $s_ret.='</ol>';
            }
            return $s_ret;
	}
	
	/**
	* Determine if mobile or not
	* EXAMPLE: [[!CommonTools? &cmd=`isMobileDevice`]]
	*
	* @return String Returns 1 or 0 (Obviously returns 1 if mobile device is detected)
	*/
	public function isMobileDevice(){
		require_once 'tools/Mobile_Detect.php';
		$detect = new Mobile_Detect();
		if($detect->isMobile()) {
			return '1';
		}else{
			return '0';
		}
	}
	
	/**
	* Get a request variable from a specific document
	* EXAMPLE: [[!CommonTools? &cmd=`getRequestVar` &varName=`postvar` &varType=`request` &default=`-`]]
	*
	* @param string $varName Name of request,post or get variable
	* @param string $varType Variable type (use either request,get or post)
	* @param string $default If variable is not set this default is returned instead (means that you dont need to wrap things in the if snippet to set a default)
	* @return String Returns a string with the variable or a blank string if not found.
	*/
	public function getRequestVar($varName,$varType,$default){
		if($varType=='get'){
			$searchArray=$_GET;
		}elseif($varType=='post'){
			$searchArray=$_POST;
		}else{
			$searchArray=$_REQUEST;
		}
		if(isset($searchArray[$varName])===true){
			return $searchArray[$varName];
		}else{
			return $default;
		}
	}
	
	/**
	* Converts a comma separated list of ids used in a wayfinder excludeDocs call and converts it to have dashes in front of each id. This can then be used in a getResources resource attribute to exlude resources.
	* EXAMPLE: [[CommonTools? &cmd=`convertExcludeDocsToGetResourcesExclude` &idString=`1,2,3`]]
	*
	* @param string $idString List of ids (comma separated)
	* @return String Returns a string with a dash in front of each id.
	*/
	public function convertExcludeDocsToGetResourcesExclude($s){
		$s=str_replace(' ','',$s);
		$a=explode(',',$s);
		
		$nArray=array();
		foreach($a as $v){
			$nArray[]='-'.$v;
		}
		return implode(',',$nArray);
	}

	/**
	* Convert the current time (or specified time) to a string date
	* EXAMPLE: [[CommonTools? &cmd=`dateFormat` &formatString=`Y-m-d` &timestamp=`1302006955`]]
	*
	* @param String $formatString Date format string to use. For an extensive syntax guide see PHPs inbuilt date function (http://php.net/manual/en/function.date.php)
	* @param number $timestamp Unix timestamp to use as the time to convert. Defaults to current time (PHPs inbuilt time function)
	* @return String Returns string that has been trimmed (or not trimmed if the length was less than the maxChars)
	*/
	public function dateFormat($formatString,$timestamp=NULL){
		if($timestamp===NULL){
			$timestamp=time();
		}
		return date($formatString,$timestamp);
	}
	
	/**
	* Convert the current time (or specified time) to a string date
	* EXAMPLE: [[CommonTools? &cmd=`stringRawUrlEncode` &string=`Hello There`]]
	*
	* @param String $string String to rawurlencode
	* @return String Returns string that has been rawurlencoded
	*/
	public function stringRawUrlEncode($string){
		return rawurlencode($string);
	}
	
	/**
	* Takes a string and trims it to maximum characters specified if the string exceeds that length
	* If the string is cut a suffix is displayed after the string (default "...")
	* EXAMPLE: [[CommonTools? &cmd=`stringTrim` &string=`A long string that needs to be trimmed` &maxChars=`20` &suffixIfCut=`...`]]
	*
	* @param String $string String to cut.
	* @param integer $maxChars Maximum characters. Any characters following this length are cut.
	* @param String $suffixIfCut A string to place at the end of the string (default: "...")
	* @return String Returns string that has been trimmed (or not trimmed if the length was less than the maxChars)
	*/
	public function stringTrim($string,$maxChars,$suffixIfCut){
		if(strlen($string)>$maxChars){
			return trim(substr($string,0,$maxChars)).$suffixIfCut;
		}else{
			return $string;
		}
	}
	
	/**
	* Search all documents and return a comma deliminated list of document IDs with a specific template
	* Very handy for product systems with a common product template
	* EXAMPLE: [[CommonTools? &cmd=`getDocumentIDsWithTemplate` &template=`3`]]
	*
	* @param integer $template Template ID to search for (Can specify multiple separated by comma).
	* @return String Comma deliminated list of document IDs
	*/
	public function getDocumentIDsWithTemplate($templates){
		$templateIds = explode(',',$templates);
		$a_ret=array();
		foreach($templateIds as $templateID){
			$documents = $this->modx->getCollection('modResource',array(
				'published'=>1,
				'template'=>$templateID
			));
			
			foreach($documents as $doc){
				if(in_array($doc->id,$a_ret)===false){
					$a_ret[]=$doc->id;
				}
			}
		}
		return implode(',',$a_ret);
	}
	
	/**
	* Creates a menu from a specific depth
	* Requires Wayfinder and UltimateParent
	* EXAMPLE: [[CommonTools? &cmd=`menuFromLevel` &topLevel=`3` &maxMenuDepth=`2`]]
	*
	* @param integer $topLevel The level that the menu will start drilling down from.
	* @param integer $maxMenuDepth Maximum levels of uls to draw.
	* @return String
	* 
	*/
	public function menuFromLevel($topLevel,$maxMenuDepth){
		$pageID = $this->modx->runSnippet('UltimateParent',array('topLevel'=>$topLevel));
		$s_menuOutput = $this->modx->runSnippet('Wayfinder',array('startId'=>$pageID,'level'=>$maxMenuDepth));
		return $s_menuOutput;
	}
	
	/**
	* Returns a page property from the current page or specified page (if set)
	* EXAMPLE: [[CommonTools? &cmd=`pageProperty` &property=`parent` &pageID=`2`]]
	*
	* @param string $property Property to retrieve from the page.
	* @param integer $pageID PageID to use. Defaults to the current page.
	* @return String Returns a string with the property (or an error).
	* 
	*/
	public function pageProperty($property,$pageID=NULL){
		if($pageID===NULL){
			$pageID=$this->modx->resource->get('id');
		}
		
		$doc = $this->modx->getObject('modResource',array(
		    'id'=>$pageID,
		));
		$s_val=$doc->$property;
		if(is_null($s_val)===false){
			return $s_val;
		}else{
			return '';
		}
	}
	
	/**
	* Returns a page property from the current page or specified page (if set)
	* EXAMPLE: [[CommonTools? &cmd=`getNextChild` &parentID=`[[*parent]]` &pageID=`[[*id]]`]]
	*
	* @param string $pageID Page ID to used in search
	* @param integer $parentID Page ID to use as a container
	* @return String Returns the id of the pageID that will come next
	* 
	*/
	function getNextChild($pageID,$parentID){
		//proceed
		$children = $this->getDocChildren($parentID,'menuindex','ASC');

		$i_nextPageID=false;
		$b_found=false;
		foreach($children as $child){
			$childID=$child->get('id');
			if($b_found===true){
				if(empty($childID)===false){
					$i_nextPageID=$childID;
				}
				break;
			}
			if($childID==$pageID){
				$b_found=true;
			}
		}
		return $i_nextPageID;
	}
	
	
	/**
	* Halts the script running and performs a header redirect to the specified page
	* EXAMPLE: [[CommonTools? &cmd=`redirectToDocument` &pageID=`2`]]
	*
	* @param integer $pageID PageID to use.
	* @return void
	*/
	public function redirectToDocument($pageID){
		if($pageID===NULL){
			$pageID=$this->modx->resource->get('id');
		}
		$url = $this->modx->makeUrl($pageID);
		$this->modx->sendRedirect($url);
	}
	
	/**
	* Halts the script running and performs a header redirect to the first child page
	* EXAMPLE: [[CommonTools? &cmd=`redirectToFirstChild`]]
	*
	* @param string $orderField Field to use to order by (defaults to menuindex)
	* @param string $orderDirection Sort order (ASC or DESC, Defaults to ASC)
	* @return void
	*/
	public function redirectToFirstChild($orderField='menuindex',$orderDirection='ASC'){
  		$children = $this->getDocChildren($this->modx->resource->get('id'),$orderField,$orderDirection);
		
		foreach($children as $child) {
			$a_fields = $child->toArray();
			$url = $this->modx->makeUrl($a_fields['id']);
			$this->modx->sendRedirect($url);
			break;
		}		
	}
	
		
	/**
	* Halts the script running and performs a header redirect to the first child page
	* EXAMPLE: [[CommonTools? &cmd=`getChildIndex` &parentID=`5` &pageID=`8` &orderField=`menuindex` &orderDirection=`ASC`]]
	*
	* @param string $parentID ParentID to use to search children 
	* @param string $pageID Child PageID to search for
	* @param string $orderField Field to use to order by (defaults to menuindex)
	* @param string $orderDirection Sort order (ASC or DESC, Defaults to ASC)
	* @return returns the child index (zero based)
	*/
	public function getChildIndex($parentID,$pageID,$orderField='menuindex',$orderDirection='ASC'){
  		$children = $this->getDocChildren($parentID,$orderField,$orderDirection);
		$_idList=array();
		foreach($children as $child) {
			$thisID=$child->get('id');
			$_idList[]=$thisID;
			if($thisID==$pageID){
				break;
			}
		}
		return count($_idList)-1;
	}
	
	/**
	* Halts the script running and performs a header redirect to the specified page
	* Requires phpthumbof snippet
	* EXAMPLE: ----- NO EXAMPLE YET -----
	*
	* @param integer $id search id variable to pass through
	* @param integer $idx search idx variable to pass through
	* @param integer $extract search extract variable to pass through
	* @param integer $img_tv template variable to use as result thumbnail
	* @param integer $img_width width of thumbnail (uses phpthumbof)
	* @param integer $img_height height of thumbnail (uses phpthumbof)
	* @return string
	*/
	public function searchResultProduct($id,$idx,$extract,$img_tv,$img_width,$img_height){
		$i_imgWidth = 200;
		$i_imgHeight = 160;
		if(isset($img_width)){$i_imgWidth=$img_width;}
		if(isset($i_imgHeight)){$i_imgHeight=$img_height;}
		
		$s_phpthumboptions='w='.$i_imgWidth.' &h='.$i_imgHeight.' &zc=1 &q=90 &f=jpg';
		
		
		$o_doc = $this->modx->getObject('modResource',$id);
		$tvValue = $o_doc->getTVValue($img_tv);
		
		$s_ret='<div class="sisea-result">';
		if(isset($tvValue)===true){
			$thumbOfImgRef = $this->modx->runSnippet("phpthumbof",array('input'=>realpath($tvValue), 'options'=>$s_phpthumboptions));
			$s_ret.='<div class="img"><img src="'.$thumbOfImgRef.'" alt="" /></div>';
		}
		$s_ret.=
		'<div class="link"><span class="searchIndex">'.$idx.'. </span><a href="[[~'.$id.']]" title="'.$o_doc->longtitle.'">'.$o_doc->pagetitle.'</a></div>
		<div class="extract"><p>'.$extract.'</p></div>
		</div>';
		
		return $s_ret;
	}
	
	/**
	* Hardcode Link
	* Returns a link directing to the page. Simple A tag link with text from the specifies field.
	* EXAMPLE: [[CommonTools? &cmd=`hardcodeLink` &pageID=`2` &titleField=`menutitle`]]
	*
	* @param integer $pageID page id of the page to link to
	* @param string $titleField field to use as the link title (can be pagetitle,longtitle,menutitle), default: menutitle
	* @return string
	*/
	public function hardcodeLink($pageID,$titleField=''){
		$o_resource = $this->modx->getObject('modResource',$pageID);
		switch($titleField){
			case 'pagetitle':
				$s_title=$o_resource->pagetitle;
				break;
			case 'longtitle':
				$s_title=$o_resource->longtitle;
				break;
			default:
				$s_title=$o_resource->menutitle;
				break;
		}
		$s_ret='<a title="'.htmlspecialchars($s_title).'" href="[[~'.$pageID.']]">'.htmlspecialchars($s_title).'</a>';
		return $s_ret;
	}
	
	/**
	* Get parent IDS
	* outputs comma separated list of parent ids relative to the pageID specified
	* EXAMPLE: [[CommonTools? &cmd=`parentIds` &pageID=`2`]]
	*
	* @param string $pageID page id of the page to get parents of
	* @return string
	*/
	public function parentIds($pageID){
		if($pageID==$this->resourceID){
			return implode(',',$this->parentIds);
		}else{
			return implode(',',$this->modx->getParentIds($pageID));
		}
	}
	

	/**
	* Hardcode Menu
	* Returns a ul menu containing links to the specified pageIds.
	* EXAMPLE: [[CommonTools? &cmd=`hardcodeMenu` &pageIDs=`2,5,8,24,25,26` &titleField=`menutitle`]]
	*
	* @param string $pageIDs page ids of the pages to link to in order (separate by comma)
	* @param string $titleField field to use as the link titles (can be pagetitle,longtitle,menutitle), default: menutitle
	* @return string
	*/
	public function hardcodeMenu($pageIDs,$titleField=''){
		$a_pageIds=explode(',',$pageIDs);
		$s_ret='<ul>';
		for($iCnt=0;$iCnt<count($a_pageIds);$iCnt++){
			$pageID=trim($a_pageIds[$iCnt]);
			
			$a_classes=array();
			
			if(in_array($pageID,$this->parentIds)===true||$pageID==$this->modx->resource->get('id')){
				$a_classes[]='active';
			}
			$a_classes[]='item'.($iCnt+1);
			if($iCnt==0){
				$a_classes[]='first';
			}
			if($iCnt==count($a_pageIds)-1){
				$a_classes[]='last';
			}
						
			$s_ret.='<li';
			if(count($a_classes)>0){
				$s_ret.=' class="'.implode(' ',$a_classes).'"';
			}
			$s_ret.='>'.$this->hardcodeLink($pageID,$titleField).'</li>';
		}
		$s_ret.='</ul>';
		return $s_ret;
	}
	
	/**
	* Facebook Share Link
	* Returns an anchor share link to the specified page (Usually you would use the current page ID)
	* Remember to add jquery to conver rel="external" to _blank targets
	* This function produced fully valid xhtml strict
	* EXAMPLE: [[CommonTools? &cmd=`facebook_shareLink` &pageID=`[[*id]]` &class=`facebookLink` &linkText=`Click here &gt;`]]
	*
	* @param integer $pageID page id to link to
	* @param string $classes classes to use on the link
	* @param string $linkText Text to use inside the anchor tag (defaults blank as you would normally use a like icon of some sort and make it a block element)
	* @return string
	*/
	public function facebook_shareLink($pageID,$classes,$linkText=''){
		$s_facebookLikeProcessor='http://www.facebook.com/sharer.php';
		$s_fullURL=$this->modx->makeUrl($pageID,'','','full');
		
		$s_ret='<a class="'.$classes.'" rel="external" href="'.$s_facebookLikeProcessor.'?u='.rawurlencode($s_fullURL).'">'.$linkText.'</a>';
		return $s_ret;
	}
	
	
	public function nextPageID($orderBy,$sortOrder,$displayTitle,$nextText,$prevText,$rotate){
		
	}
	
	/**
	* Used to create Previous and Next links to the next page in the current folder location
	* EXAMPLE: [[!CommonTools? &cmd=`nextPrevLinks` &orderBy=`menuindex` &sortOrder=`ASC` &displayTitle=`0` &nextText=`Next &gt;` &prevText=`&lt; Previous` &rotate=`1`]] 
	*
	* @param string $orderBy Order to sort the page lists (menuindex, pagetitle, createdon etc)
	* @param string $sortOrder Direct of the sort (ASC, DESC)
	* @param boolean $displayTitle Display title of page as link text or simply use previous, next text
	* @param string $nextText Text to use as next link
	* @param string $prevText Text to use as previous link
	* @param boolean $rotate If the last item in list make Next go to first item and if the first item make previous go to the last item
	* @return string
	*/
	public function nextPrevLinks($orderBy,$sortOrder,$displayTitle,$nextText,$prevText,$rotate){	
		// Get the parent ID
		$parentid = $this->modx->resource->get('parent'); // maybe this solves it all, but I'm not sure.
		
		// (re)set the $id variable to the one of this document.
		$id = $this->modx->resource->get('id');
		
		$children = $this->getDocChildren($parentid,$orderBy,$sortOrder);
		
		// the number of selected documents
		$limit = count($children);
		
		// set $y to zero
		$y = 0;
		
		// sorting the documents, giving them a sequential and searchable index
		$my_array=array();
		foreach ($children as $child) {
			$my_array[$y] = $child->toArray();
		    if ($my_array[$y]['id']==$id) {
		        $current=$y; // The current page has number $y in the array
		    }
		    $y++;
		}

		$prev = $current-1;
		$next = $current+1;
		if($rotate==true){
			if($next>count($my_array)-1){
				$next=0;
			}
			if($prev<0){
				$prev=count($my_array)-1;
			}
		}

		// Here starts the output
		$output='';
		$output.="<div class='prevNextMenu'>";
		
		if($prev<0){
			//dont show because we are at the first item already
		}else{
			if($displayTitle==false){
			    $output.='<a class="prev" title="'.$my_array[$prev]['pagetitle'].'" href="[[~'.$my_array[$prev]['id'].']]">'.$prevText.'</a>';
			}elseif($displayTitle==true) {
			    $output .= '<a class="prev" title="'.$my_array[$prev]['pagetitle'].'" href="[[~'.$my_array[$prev]['id'].']]">'.$my_array[$prev]['pagetitle'].'</a>';
			}
		}
		
		if($next>count($my_array)-1){
			//dont show because we are at the last item already
		}else{
			if($displayTitle==false){
			    $output .= '<a class="next" title="'.$my_array[$next]['pagetitle'].'" href="[[~'.$my_array[$next]['id'].']]">'.$nextText.'</a>';
			} elseif($next <= $limit && $displayTitle==true) {
			    $output .='<a class="next" title="'.$my_array[$next]['pagetitle'].'" href="[[~'.$my_array[$next]['id'].']]">'.$my_array[$next]['pagetitle'].'</a>';
			}
		}
		
		$output.="</div>";
		
		return $output;
	}
	/**
	* Used to output a list of thumbnails with links to the full image from a specified directory path
	* EXAMPLE: [[!CommonTools? &cmd=`gallery_directoryThumbLinks` &path=`/folder/for/images` &thumbWidth=`100`  &thumbHeight=`80` ]] 
	* Requires phpthumbof snippet
	*
	* @param string $orderBy Order to sort the page lists (menuindex, pagetitle, createdon etc)
	* @param string $sortOrder Direct of the sort (ASC, DESC)
	* @param boolean $displayTitle Display title of page as link text or simply use previous, next text
	* @param string $nextText Text to use as next link
	* @param string $prevText Text to use as previous link
	* @param boolean $rotate If the last item in list make Next go to first item and if the first item make previous go to the last item
	* @return string
	*/
	public function gallery_directoryThumbLinks($path,$thumbWidth,$thumbHeight,$wrapperStart,$wrapperEnd,$anchorAttributes){
		if(!empty($anchorAttributes)){
			$anchorAttributes=' '.$anchorAttributes.' ';
		}
		$s_phpthumboptions='w='.$thumbWidth.' &h='.$thumbHeight.' &zc=1 &q=90 &f=jpg';
		$s_ret='';
		$a_validImgExt=array('jpg','gif','png','jpeg');
		$path = dirname($path);
		if($handle = opendir(MODX_BASE_PATH.$path)) {
		    /* This is the correct way to loop over the directory. */
		    while (false !== ($file = readdir($handle))){
		    	foreach($a_validImgExt as $s_ext){
		    		$i_lenExt=strlen($s_ext);
					//echo substr($file,strlen($file)-$i_lenExt-1,$i_lenExt+1).'<br />';
					//+1 -1 for the extra "."
		    		if(strtolower(substr($file,strlen($file)-$i_lenExt-1,$i_lenExt+1))=='.'.$s_ext){
		    			$s_fullPath = $path.'/'.$file;
						$s_thumbOfImgRef = $this->modx->runSnippet("phpthumbof",array('input'=>$s_fullPath, 'options'=>$s_phpthumboptions));					

		    			$s_ret.=$wrapperStart
						.'<a href="'.$s_fullPath.'"'.$anchorAttributes.'>'
						.'<img src="'.$s_thumbOfImgRef .'" alt="" />'
						.'</a>'
		    			.$wrapperEnd;
						break;
		    		}
		    	}		        
		    }
		    closedir($handle);
		}
		
		
		return $s_ret;
	}

	
	/**
	* Debug Processing Tiome
	* EXAMPLE: [[!CommonTools? &cmd=`debug_processTime` &processTrackId=`6354`, &note=`blah blah`]] 
	*/
	public function debug_processTime($processTrackId,$note,$output){
		$b_started=false;
		if(isset($GLOBALS['debugprocesstime_'.$processTrackId])===false){
			$GLOBALS['debugprocesstime_'.$processTrackId]=array(array('time'=>microtime(true),'note'=>$note)); //add current millisecond (micro is one millionth)
			$b_started=true;
		}
		if($b_started===false){
			$GLOBALS['debugprocesstime_'.$processTrackId][]=array('time'=>microtime(true),'note'=>$note); //add current millisecond (micro is one millionth)
		}
		$lb="\r\n";
		$s_ret=$lb.'<ol style="display:none;">';
		$lastTime=$GLOBALS['debugprocesstime_'.$processTrackId][0]['time'];
		foreach($GLOBALS['debugprocesstime_'.$processTrackId] as $item){
			$time=$item['time'];
			$s_ret.=$lb.'<li>'.round($time-$lastTime,3).'secs'.(strlen($item['note'])>0?' ('.$item['note'].')':'').'</li>';
			$lastTime=$time;
		}
		$s_ret.=$lb.'<li>Total: '.round($lastTime-$GLOBALS['debugprocesstime_'.$processTrackId][0]['time'],3).'secs</li>';
		$s_ret.=$lb.'</ol>'.$lb;
		if($output===true){
			return $s_ret;
		}else{
			return '';
		}
	}

	/**
	* Used to return a list of pageIDs that match a specified template variable numebr range (used for selecting all products in price range)
	* EXAMPLE: [[!CommonTools? &cmd=`getDocumentsWithTvNumberRange` &templateVar=`Price` &priceLow=`0.05` &priceHigh=`9.99`]] 
	*
	* @param float $priceLow Minimum price to select
	* @param float $priceHigh Maximum price to select
	* @param boolean $templateVar Name of template variable
	* @return string
	*/
	public function getDocumentsWithTvNumberRange($priceLow,$priceHigh,$templateVar){
		//returns a comma separated string of active document ids with a specific template (recursive)
		//must pass a varible called "template" (id of the template)
		$a_ret=array();
		$o_doc = $this->modx->getObject('modTemplateVar',array('name'=>$templateVar));
		
		$query = $this->modx->newQuery('modTemplateVarResource');
		//select all columns
		$query->select($this->modx->getSelectColumns('modTemplateVarResource', '', '', array('id','tmplvarid','contentid','value')));
		$query->where(array(
			'tmplvarid'=>$o_doc->get('id')
		));
		$tvarPrices = $this->modx->getCollection('modTemplateVarResource',$query);
		
		$a_pageIDs=array();
		foreach($tvarPrices as $tvarPrice) {
			$a_fields = $tvarPrice->toArray();
			if(is_numeric($a_fields['value'])){
				$f_val=(Float)$a_fields['value'];
				$i_pageID=(int)$a_fields['contentid'];
				
				if($f_val>=$priceLow && $f_val<=$priceHigh){
					if(in_array($i_pageID,$a_pageIDs)===false){
						$a_pageIDs[]=$a_fields['contentid'];
					}
				}
			}
		}
		$s_ret=implode(',',$a_pageIDs);
		return $s_ret;
	}
	
	/**
	* Used to return a list of pageIDs that match a specified template variable value
	* EXAMPLE: [[!CommonTools? &cmd=`getDocumentsWithTvValue` &templateVar=`Price` &value=`yes` &prefixNeg=`0`]] 
	*
	* @param string $value Value to match
	* @param string $templateVar Name of template variable
	* @param boolean $prefixNeg USed for gerresources, set to tru to add a - sign to each entry to exclude with getResources
	* @return string
	*/
	public function getDocumentsWithTvValue($value,$templateVar,$prefixNeg){
		//returns a comma separated string of active document ids with a specific template (recursive)
		//must pass a varible called "template" (id of the template)
		$a_ret=array();
		$o_doc = $this->modx->getObject('modTemplateVar',array('name'=>$templateVar));
		
		$query = $this->modx->newQuery('modTemplateVarResource');
		//select all columns
		$query->select($this->modx->getSelectColumns('modTemplateVarResource', '', '', array('id','tmplvarid','contentid','value')));
		$query->where(array(
			'tmplvarid'=>$o_doc->get('id')
		));
		$tvarPrices = $this->modx->getCollection('modTemplateVarResource',$query);
		
		$a_pageIDs=array();
		foreach($tvarPrices as $tvarPrice) {
			$a_fields = $tvarPrice->toArray('',false,true);
			if($a_fields['value']==$value){
				if($prefixNeg===true){
					$a_pageIDs[]='-'.$a_fields['contentid'];
				}else{
					$a_pageIDs[]=$a_fields['contentid'];
				}
				
			}
		}
		$s_ret=implode(',',$a_pageIDs);
		return $s_ret;
	}

	/**
	* Debug Processing Tiome
	* EXAMPLE: [[!CommonTools? &cmd=`getFirstChildWithChildren` &pageID=`6354` &orderField=`menuindex` &orderDirection=`ASC`]] 
	*/
	public function getFirstChildWithChildren($pageID,$orderField='menuindex',$orderDirection='ASC'){
		
		$c = $this->modx->newQuery('modResource');
		$c->sortby($orderField,$orderDirection);
		$c->where(array('published'=>1, 'parent' => $pageID));
  		$children = $this->modx->getCollection('modResource',$c);
		
		foreach($children as $child){
			$childID = $child->get('id');
			$innerQ = $this->modx->newQuery('modResource');
			$innerQ->sortby($orderField,$orderDirection);
			$innerQ->where(array('published'=>1, 'parent' => $childID));
			
			$innerChilds = $this->modx->getCollection('modResource',$innerQ);
			if(count($innerChilds)>0){
				return $childID;
			}
		}
		return '-100'; //-100 should never return any results in a get resources call
	}

	/**
	* User Logged In
	* EXAMPLE: [[!CommonTools? &cmd=`user_isloggedIn`]]
	* @return string True 1 or False 0
	*/
	public function user_isloggedIn(){
		$s_ret='0';
		if($this->modx->user->hasSessionContext($this->modx->context->get('key'))===true){
			$s_ret='1';
		}
		return $s_ret;
	}
	
	/**
	* ALWAYS call uncached - made to be called from a snippet directly
	* @return string
	*/
	public function smartCache($snippet,$args){
		/*
		$cacheOptions = array(
			xPDO::OPT_CACHE_KEY => 'commontools',
			xPDO::OPT_CACHE_HANDLER => $this->modx->getOption('cache_resource_handler',NULL, 'xPDOFileCache')
		);
		*/
		$usergroups =  $this->modx->user->getUserGroups();
		$cacheKey = $this->modx->getOption('cache_resource_key', null, 'resource/commontools/smartcache/'.$snippet);
		$cacheExpires = (integer) $this->modx->getOption('cache_resource_expires', null, $this->modx->getOption(xPDO::OPT_CACHE_EXPIRES, null, 0));
		$cacheHandler = $this->modx->getOption('cache_resource_handler', null, $this->modx->getOption(xPDO::OPT_CACHE_HANDLER, null, 'xPDOFileCache'));

		$cacheOptions = array(
			xPDO::OPT_CACHE_KEY => $cacheKey,
			xPDO::OPT_CACHE_HANDLER => $cacheHandler,
			xPDO::OPT_CACHE_EXPIRES => $cacheExpires,
		);

		$cacheFile = '';
		foreach($args as $arg){
			$cacheFile.='_'.$arg;
		}
		$cacheFile.='_grps_'.implode('_',$usergroups);
		$cachedResult = $this->modx->cacheManager->get($cacheFile,$cacheOptions);
		
		if(!empty($cachedResult)){
			return $cachedResult;
		}else{
			$s_ret = $this->modx->runSnippet($snippet,$args);
			// CUSTOM CACHE WRITE
			$this->modx->cacheManager->set($cacheFile,$s_ret,NULL,$cacheOptions);
			return $s_ret;
		}
	}
	
        //Via PHP you can set a unique key and string to retrieve something from the resource cache
	//Cleared when content is updated.
        public function cacheStringGet($key,$cachePath='resource/commontools/cachestring/'){
            $usergroups =  $this->modx->user->getUserGroups();
            $cacheKey = $this->modx->getOption('cache_resource_key', null, $cachePath.$key);
            $cacheExpires = (integer) $this->modx->getOption('cache_resource_expires', null, $this->modx->getOption(xPDO::OPT_CACHE_EXPIRES, null, 0));
            $cacheHandler = $this->modx->getOption('cache_resource_handler', null, $this->modx->getOption(xPDO::OPT_CACHE_HANDLER, null, 'xPDOFileCache'));

            $cacheOptions = array(
                    xPDO::OPT_CACHE_KEY => $cacheKey,
                    xPDO::OPT_CACHE_HANDLER => $cacheHandler,
                    xPDO::OPT_CACHE_EXPIRES => $cacheExpires,
            );

            $cacheFile = '';
            $cacheFile.='_grps_'.implode('_',$usergroups);
            $cachedResult = $this->modx->cacheManager->get($cacheFile,$cacheOptions);

            if(!empty($cachedResult)){
                    return $cachedResult;
            }else{
                return false;
            }
	}
        //Via PHP you can set a unique key and string to retrieve something from the resource cache
	//Cleared when content is updated.
	public function cacheStringSet($key,$s_content,$cachePath='resource/commontools/cachestring/'){
            $usergroups =  $this->modx->user->getUserGroups();
            $cacheKey = $this->modx->getOption('cache_resource_key', null, $cachePath.$key);
            $cacheExpires = (integer) $this->modx->getOption('cache_resource_expires', null, $this->modx->getOption(xPDO::OPT_CACHE_EXPIRES, null, 0));
            $cacheHandler = $this->modx->getOption('cache_resource_handler', null, $this->modx->getOption(xPDO::OPT_CACHE_HANDLER, null, 'xPDOFileCache'));

            $cacheOptions = array(
                    xPDO::OPT_CACHE_KEY => $cacheKey,
                    xPDO::OPT_CACHE_HANDLER => $cacheHandler,
                    xPDO::OPT_CACHE_EXPIRES => $cacheExpires,
            );

            $cacheFile = '';
            $cacheFile.='_grps_'.implode('_',$usergroups);
            $this->modx->cacheManager->set($cacheFile,$s_content,NULL,$cacheOptions);
	}
	
	/**
	* User Logged In
	* EXAMPLE: [[!CommonTools? &cmd=`getHttpType`]]
	* @return string http or https
	*/
	public function getHttpType(){
		$ssl = false;
		if ($_SERVER['HTTPS'] == 1) {
			$ssl = true;
		} elseif ($_SERVER['HTTPS'] == 'on') {
			$ssl = true;
		} elseif ($_SERVER['SERVER_PORT'] == 443) {
			$ssl = true;
		}
		if($ssl){
			return 'https';
		}else{
			return 'http';
		}
	}
        
        function noRobotsForHttps(){
            $ssl = false;
            if ($_SERVER['HTTPS'] == 1) {
                    $ssl = true;
            } elseif ($_SERVER['HTTPS'] == 'on') {
                    $ssl = true;
            } elseif ($_SERVER['SERVER_PORT'] == 443) {
                    $ssl = true;
            }
            if($ssl){
                return '<meta name="robots" content="noindex"/>';
            }
        }
        
        function cannonicalURL(){
            $id = $this->modx->resource->id;
            return strtolower($this->modx->makeUrl($id,'','','http'));
        }
        
        function URLNoSlash(){
            $input = $this->modx->getOption('site_url');
            $length = strlen($input);
            $s_ret = substr($input,0,$length-1);
            return $s_ret;
        }
        
        //handy function to quickly loop any users in a specific group
        public static function getUsersInGroup($modx,$i_group){
            $c = $modx->newQuery('modUser');
            $c->innerJoin ('modUserProfile','Profile');
            $c->innerJoin ('modUserGroupMember','UserGroupMembers');
            $c->innerJoin ('modUserGroup','UserGroup','`UserGroupMembers`.`user_group` = `UserGroup`.`id`');
            $c->leftJoin ('modUserGroupRole','UserGroupRole','`UserGroupMembers`.`role` = `UserGroupRole`.`id`');
            $c->where(array(
            'active' => true,
            'UserGroupMembers.user_group' => $i_group,
            ));
            $ret_result = $modx->getCollection('modUser',$c);
            return $ret_result;
        }
}